﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Models
{
    public class ReturnedFacets
    {
        public List<KeyValuePair<string, int>> SpecialtyFacet { get; set; }

        public List<KeyValuePair<string, int>> GenderFacet { get; set; }
    }
}