﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Models
{
    public class CurrentFacets
    {
        public List<string> SpecialtyFacet { get; set; }
        public List<string> GenderFacet { get; set; }
    }
}