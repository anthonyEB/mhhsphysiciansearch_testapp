﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Models
{
    public class Typeahead
    {
        public string label { get; set; }
        public string value { get; set; }
        public string link { get; set; }
        public string type { get; set; }
    }
}