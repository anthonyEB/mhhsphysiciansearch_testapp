﻿using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Models
{
    public class Physician
    {
        [SolrUniqueKey("firstname_s")]
        public string FirstName { get; set; }

        [SolrField("lastname_s")]
        public string LastName { get; set; }

        [SolrField("degree_s")]
        public string Degree { get; set; }

        [SolrField("gender_s")]
        public string Gender { get; set; }

        [SolrField("specialty_s")]
        public string Specialty { get; set; }

        [SolrField("_fullpath")]
        public string FullPath { get; set; }

        [SolrField("_uniqueid")]
        public string ItemID { get; set; }

        [SolrField("_name")]
        public List<string> MSOID { get; set; }
    }
}