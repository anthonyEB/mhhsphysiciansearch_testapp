﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Models
{
    public class PhysicianQuery
    {
        public PhysicianQuery()
        {
            Rows = 10;
            Start = 0;
            SpecialtyFilter = new List<string>();
            GenderFilter = new List<string>();
        }
        //Query object that holds parameters sent to solr
        public string Query { get; set; }

        public int Start { get; set; }

        public int Rows { get; set; }
        
        public List<string> SpecialtyFilter { get; set; }

        public List<string> GenderFilter { get; set; }
    }
}