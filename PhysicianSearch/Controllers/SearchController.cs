﻿using PhysicianSearch.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhysicianSearch.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getViews()
        {
            // get params
            string q = Request.Params.Get("query");
            string specialties = Request.Params.Get("specialty");
            string genders = Request.Params.Get("gender");

            // build query
            Respository.ExecuteSearch searchLibrary = new Respository.ExecuteSearch();
            PhysicianQuery query = new PhysicianQuery();
            query.Query = q;

            // add filters
            if (specialties != null && specialties != "")
            {
                List<string> s = specialties.Split(',').ToList();
                foreach (string item in s)
                {
                    query.SpecialtyFilter.Add(item);
                }
            }

            if (genders != null && genders != "")
            {
                List<string> g = genders.Split(',').ToList();
                foreach (string item in g)
                {
                    query.GenderFilter.Add(item);
                }
            }

            // query solr
            QueryResponse response = searchLibrary.DoSearch(query);

            // load model
            Search model = new Search();
            model.Physicians = response.Results;
            model.currentFacets.SpecialtyFacet = specialties.Split(',').ToList();
            model.currentFacets.GenderFacet = genders.Split(',').ToList();
            model.returnedFacets.SpecialtyFacet = response.SpecialtyFacet;
            model.returnedFacets.GenderFacet = response.GenderFacet;
            model.Start = query.Start;
            model.Rows = query.Rows;
            model.TotalDocs = response.TotalHits;

            // return partials as json
            var searchResultsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_SearchResults", model);
            var specialtiesFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_SpecialtiesFacets", model);
            var genderFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_GenderFacets", model);
            var paginationView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_Pagination", model);
            var json = Json(new { searchResultsView, specialtiesFacetsView, genderFacetsView, paginationView });
            return json;
        }

        [HttpPost]
        public JsonResult getTypeahead()
        {
            // get params
            string q = Request.Params.Get("query");

            // build query
            Respository.ExecuteSearch searchLibrary = new Respository.ExecuteSearch();
            PhysicianQuery query = new PhysicianQuery();
            query.Query = q;

            // query solr
            QueryResponse response = searchLibrary.DoTypeahead(query);

            // load model
            Search model = new Search();
            model.Physicians = response.Results;

            List<Typeahead> ta = new List<Typeahead>();
            foreach (var item in model.Physicians) {
                Typeahead test = new Typeahead();
                test.label = item.FirstName + " " + item.LastName + " " + item.Degree;
                test.value = item.FirstName + " " + item.LastName;
                test.link = item.FullPath;
                test.type = "physician";
                ta.Add(test);
            }

            return Json(ta);
        }

        [HttpPost]
        public JsonResult getFacets()
        {

            // build query
            Respository.ExecuteSearch searchLibrary = new Respository.ExecuteSearch();
            PhysicianQuery query = new PhysicianQuery();
            query.Query = "*:*";

            // query solr
            QueryResponse response = searchLibrary.DoSearch(query);

            // load model
            //Search model = new Search();
            //model.returnedFacets.SpecialtyFacet = response.SpecialtyFacet;
            //model.returnedFacets.GenderFacet = response.GenderFacet;

            List<KeyValuePair<string, List<Typeahead>>> facets = new List<KeyValuePair<string, List<Typeahead>>>();

            List<Typeahead> specialty = new List<Typeahead>();
            foreach (var item in response.SpecialtyFacet)
            {
                Typeahead ta = new Typeahead();
                ta.label = item.Key;
                ta.value = item.Key;
                ta.link = "";
                ta.type = "specialty";
                specialty.Add(ta);
            }
            facets.Add(new KeyValuePair<string, List<Typeahead>>("Specialty", specialty));

            return Json(facets);
        }
    }
}