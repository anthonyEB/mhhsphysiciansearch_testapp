﻿using PhysicianSearch.Models;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Respository
{
    internal class FiltersFacets
    {
        // Filters
        internal ICollection<ISolrQuery> BuildFilterQueries(PhysicianQuery query)
        {
            ICollection<ISolrQuery> filters = new List<ISolrQuery>();
            List<SolrQueryByField> filtersSpecialty = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersGender = new List<SolrQueryByField>();

            foreach (string specialty in query.SpecialtyFilter)
            {
                filtersSpecialty.Add(new SolrQueryByField("specialty_s", specialty));
            }

            foreach (string gender in query.GenderFilter)
            {
                filtersGender.Add(new SolrQueryByField("gender_s", gender));
            }

            if (filtersSpecialty.Count > 0)
            {
                filters.Add(new LocalParams {{ "tag", "st" }} + new SolrMultipleCriteriaQuery(filtersSpecialty, "OR"));
            }

            if (filtersGender.Count > 0)
            {
                filters.Add(new LocalParams {{ "tag", "gt" }} + new SolrMultipleCriteriaQuery(filtersGender, "OR"));
            }

            return filters;
        }

        // Facets
        internal FacetParameters BuildFacets()
        {
            return new FacetParameters
            {
                Queries = new List<ISolrFacetQuery>
                {
                    new SolrFacetFieldQuery(new LocalParams {{ "ex", "st" }} + "specialty_s") { MinCount = 1 },
                    new SolrFacetFieldQuery(new LocalParams {{ "ex", "gt" }} + "gender_s") { MinCount = 1 }
                }
            };
        }
    }
}