﻿using Microsoft.Practices.ServiceLocation;
using PhysicianSearch.Models;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Respository
{
    public class ExecuteSearch
    {
        public QueryResponse DoSearch(PhysicianQuery query)
        {
            //Filters
            FiltersFacets filtersFacets = new FiltersFacets();
            
            //Create an object to hold results
            SolrQueryResults<Physician> solrResults;
            QueryResponse queryResponse = new QueryResponse();

            //Echo back the original query 
            queryResponse.OriginalQuery = query;

            //Get a connection (swapped to executor for support for different request handler)
            ISolrOperations<Physician> solr = ServiceLocator.Current.GetInstance<ISolrOperations<Physician>>();
            //var executor = ServiceLocator.Current.GetInstance<ISolrQueryExecuter<Physician>>() as SolrQueryExecuter<Physician>;

            //executor.Handler = "/suggest";

            //testing pagination
            //query.Rows = 2;

            //Set Options
            QueryOptions queryOptions = new QueryOptions
            {
                Rows = query.Rows,
                StartOrCursor = new StartOrCursor.Start(query.Start),
                FilterQueries = filtersFacets.BuildFilterQueries(query),
                Facet = filtersFacets.BuildFacets(),
            };

            //Execute the query
            ISolrQuery solrQuery = new SolrQuery(query.Query);

            //swap to executor
            solrResults = solr.Query(solrQuery, queryOptions);
            //solrResults = executor.Execute(solrQuery, queryOptions);

            //Set response
            ResponseExtraction extractResponse = new ResponseExtraction();

            extractResponse.SetHeader(queryResponse, solrResults);
            extractResponse.SetBody(queryResponse, solrResults);
            extractResponse.SetFacets(queryResponse, solrResults);

            //Return response;
            return queryResponse;
        }

        public QueryResponse DoTypeahead(PhysicianQuery query)
        {
            //Create an object to hold results
            SolrQueryResults<Physician> solrResults;
            QueryResponse queryResponse = new QueryResponse();

            //Echo back the original query 
            queryResponse.OriginalQuery = query;

            //Get a connection (swapped to executor for support for different request handler)
            var executor = ServiceLocator.Current.GetInstance<ISolrQueryExecuter<Physician>>() as SolrQueryExecuter<Physician>;

            //Use suggest handler
            executor.Handler = "/suggest";

            //may want to suggest less
            //query.Rows = 5;

            //Set Options
            QueryOptions queryOptions = new QueryOptions
            {
                Rows = query.Rows,
                StartOrCursor = new StartOrCursor.Start(query.Start),
            };

            //Execute the query
            ISolrQuery solrQuery = new SolrQuery(query.Query);

            //swap to executor
            //solrResults = solr.Query(solrQuery, queryOptions);
            solrResults = executor.Execute(solrQuery, queryOptions);

            //Set response
            ResponseExtraction extractResponse = new ResponseExtraction();

            extractResponse.SetHeader(queryResponse, solrResults);
            extractResponse.SetBody(queryResponse, solrResults);

            //Return response;
            return queryResponse;
        }
    }
}