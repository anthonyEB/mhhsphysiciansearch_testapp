﻿using PhysicianSearch.Models;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhysicianSearch.Respository
{
    public class ResponseExtraction
    {
        //Extract parts of the SolrNet response and set them in QueryResponse class
        internal void SetHeader(QueryResponse queryResponse, SolrQueryResults<Physician> solrResults)
        {
            queryResponse.QueryTime = solrResults.Header.QTime;
            queryResponse.Status = solrResults.Header.Status;
            queryResponse.TotalHits = solrResults.NumFound;
        }

        internal void SetBody(QueryResponse queryResponse, SolrQueryResults<Physician> solrResults)
        {
            queryResponse.Results = (List<Physician>)solrResults;
        }

        internal void SetFacets(QueryResponse queryResponse, SolrQueryResults<Physician> solrResults)
        {
            // Specialties
            if (solrResults.FacetFields.ContainsKey("specialty_s"))
            {
                queryResponse.SpecialtyFacet = solrResults.FacetFields["specialty_s"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }

            // Gender
            if (solrResults.FacetFields.ContainsKey("gender_s"))
            {
                queryResponse.GenderFacet = solrResults.FacetFields["gender_s"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }
        }
    }
}