﻿using PhysicianSearch.Models;
using SolrNet;
using SolrNet.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PhysicianSearch
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Startup.Init<Physician>("http://localhost:8983/solr/sitecore_physician_index");
            //Startup.Init<Physician>("http://mcedwwebdev01:8983/solr/sitecore_physician_index");

            Startup.Init<Physician>(new SolrConnection("http://localhost:8983/solr/sitecore_physician_index"));
        }
    }
}
